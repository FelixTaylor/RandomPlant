package bib.rollntaste;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import bib.rollntaste.database.Handler;

public class MainActivity extends AppCompatActivity {

    private Resources res;
    private Random random;
    private Handler handler;

    private Dialog add_dialog;
    private boolean[] locks;
    private TextView[] values;
    private EditText input;
    private ImageView[] lockImages;

    private TextView last;
    private int rowId;

    private ArrayList<String> tastes, colors, parts, consistencies;
    private ClickListener clickListener;

    private class ClickListener implements View.OnClickListener {
        @Override public void onClick(View view) {
            if (input.getText().toString().trim().isEmpty()) {
                input.setError(res.getString(R.string.input_required));
            } else {
                if (isValid(input, rowId)) {
                    handler.open();

                    switch (rowId){
                        case 0: {
                            handler.insertPart(input.getText().toString());
                            parts = handler.selectPart();
                            showToast(input, res.getString(R.string.add_to_part));
                            break;
                        }
                        case 1: {
                            handler.insertColor(input.getText().toString());
                            colors = handler.selectColor();
                            showToast(input, res.getString(R.string.add_to_color));
                            break;
                        }
                        case 2: {
                            handler.insertTaste(input.getText().toString());
                            tastes = handler.selectTaste();
                            showToast(input, res.getString(R.string.add_to_taste));
                            break;
                        }
                        case 3: {
                            handler.insertConsistency(input.getText().toString());
                            consistencies = handler.selectConsistency();
                            showToast(input, res.getString(R.string.add_to_consistency));
                            break;
                        }
                    }

                    handler.close();
                    input.setText("");
                    add_dialog.dismiss();
                } else {
                    showToast(input, res.getString(R.string.does_exist));
                }
            }
        }
    }
    private class ResetListener implements View.OnClickListener {
        @Override public void onClick(View view) {

            final Dialog reset_dialog = new Dialog(MainActivity.this);
            reset_dialog.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);

            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
                reset_dialog.setContentView(R.layout.dialog_reset_db_ap21);
            } else {
                reset_dialog.setContentView(R.layout.dialog_reset_db_api19);
                reset_dialog.setTitle(R.string.tv_teset_info);
            }

            Button btn_reset, btn_cancle;
            btn_reset = (Button) reset_dialog.findViewById(R.id.btn_reset);
            btn_reset.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    handler.open();
                    handler.reset();
                    handler.close();
                    updateList();
                    Toast.makeText(
                            getApplicationContext(),
                            "Datenbank zurückgesetzt!",
                            Toast.LENGTH_SHORT
                    ).show();
                    reset_dialog.dismiss();
                }
            });

            btn_cancle = (Button) reset_dialog.findViewById(R.id.btn_cancle);
            btn_cancle.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    reset_dialog.dismiss();
                }
            });

            reset_dialog.show();
        }
    }
    private class RowLongClickListener implements View.OnLongClickListener {
        @Override public boolean onLongClick(View view) {
            Button btn_add, btn_cancle;
            btn_add = (Button) add_dialog.findViewById(R.id.btn_add);
            input = (EditText) add_dialog.findViewById(R.id.inputfield);

            switch (view.getId()) {

                case 0: {
                    input.setHint(
                            res.getString(R.string.tv_part)
                            + " " + res.getString(R.string.add)
                    );
                    rowId = 0;
                    btn_add.setOnClickListener(clickListener);
                    break;
                }

                case 1: {
                    input.setHint(
                            res.getString(R.string.tv_color)
                            + " " + res.getString(R.string.add)
                    );
                    rowId = 1;
                    btn_add.setOnClickListener(clickListener);
                    break;
                }

                case 2: {
                    input.setHint
                            (res.getString(R.string.tv_taste)
                            + " " + res.getString(R.string.add)
                            );
                    rowId = 2;
                    btn_add.setOnClickListener(clickListener);
                    break;
                }

                case 3: {
                    input.setHint(
                            res.getString(R.string.tv_consistency)
                            + " " + res.getString(R.string.add)
                    );
                    rowId = 3;
                    btn_add.setOnClickListener(clickListener);
                    break;
                }
            }

            btn_cancle = (Button) add_dialog.findViewById(R.id.btn_cancle);
            btn_cancle.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    add_dialog.dismiss();
                    input.setText("");
                }
            });
            add_dialog.show();
            return false;
        }
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                random();
            }
        });

        res = getResources();
        random = new Random();
        handler = new Handler(this);
        last = (TextView) findViewById(R.id.tv_last);

        tastes = new ArrayList<>();
        colors = new ArrayList<>();
        parts = new ArrayList<>();
        consistencies = new ArrayList<>();

        updateList();

        TableLayout table = (TableLayout) this.findViewById(R.id.tableLayout);
        String[] titles = {
                res.getString(R.string.tv_part),
                res.getString(R.string.tv_color),
                res.getString(R.string.tv_taste),
                res.getString(R.string.tv_consistency)
        };
        values = new TextView[titles.length];
        locks = new boolean[titles.length];
        lockImages = new ImageView[titles.length];
        clickListener = new ClickListener();

        Button resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new ResetListener());

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RowLongClickListener longClickListener = new RowLongClickListener();

        add_dialog = new Dialog(MainActivity.this);
        add_dialog.requestWindowFeature(Window.FEATURE_OPTIONS_PANEL);
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
            add_dialog.setContentView(R.layout.dialog_add_to_db_ap21);
        } else {
            add_dialog.setContentView(R.layout.dialog_add_to_db_api19);
            add_dialog.setTitle(R.string.btn_add);
        }




        for (int i=0; i<titles.length; i++) {
            View row = inflater.inflate(R.layout.layout_row, null);

            LinearLayout layout = (LinearLayout) row.findViewById(R.id.row_header);
            layout.setId(i);
            layout.setOnLongClickListener(longClickListener);

            TextView title = (TextView) row.findViewById(R.id.row_title);
            title.setText(titles[i]);

            values[i] = (TextView) row.findViewById(R.id.row_value);
            values[i].setText(R.string.placeholder);

            lockImages[i] = (ImageView) row.findViewById(R.id.row_lock);
            lockImages[i].setId(i);

            if (i%2==0) { layout.setBackgroundColor(res.getColor(R.color.colorAccentDark)); }
            table.addView(row);
        }
    }
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override protected void onResume() {
        super.onResume();
        updateList();
    }
    @Override protected void onPause() {
        super.onPause();
        handler.close();
    }

    public void lock(View view) {
        int id = view.getId();
        if (!values[id].getText().equals(res.getString(R.string.placeholder))) {
            if (locks[id]) {
                lockImages[id].setImageDrawable(
                        res.getDrawable(R.mipmap.ic_lock_open_white_24dp)
                );
                locks[id] = false;
            } else {
                lockImages[id].setImageDrawable(
                        res.getDrawable(R.mipmap.ic_lock_outline_white_24dp)
                );
                locks[id] = true;
            }
        }
    }
    private void random() {
        if (!values[0].getText().toString().equals(res.getString(R.string.placeholder)) &&
                !values[1].getText().toString().equals(res.getString(R.string.placeholder)) &&
                !values[2].getText().toString().equals(res.getString(R.string.placeholder)) &&
                !values[3].getText().toString().equals(res.getString(R.string.placeholder)))
        {
            last.setText(
                    new StringBuffer()
                            .append("Letzte Pflanze:\n")
                            .append(values[0].getText().toString().toUpperCase())
                            .append(", ")
                            .append(values[1].getText().toString().toUpperCase())
                            .append(", ")
                            .append(values[2].getText().toString().toUpperCase())
                            .append("/ ")
                            .append(values[3].getText().toString().toUpperCase())
            );
        }

        if (!locks[0]) {
            values[0].setText(parts.get(random.nextInt(parts.size())));
        }

        if (!locks[1]) {
            values[1].setText(colors.get(random.nextInt(colors.size())));
        }

        if (!locks[2]) {
            values[2].setText(tastes.get(random.nextInt(tastes.size())));
        }

        if (!locks[3]) {
            values[3].setText(consistencies.get(random.nextInt(consistencies.size())));
        }
    }
    private void updateList() {
        handler.open();
        parts = handler.selectPart();
        colors = handler.selectColor();
        tastes = handler.selectTaste();
        consistencies = handler.selectConsistency();
        handler.close();
    }
    private boolean isValid(TextView tv, int id) {
        String value = tv.getText().toString();
        boolean isValid = true;
        switch (id) {
            case 0: {
                for (String s : parts) {
                    if (s.toLowerCase().equals(value)) {
                        isValid = false;
                    }
                }
                break;
            }

            case 1: {
                for (String s : colors) {
                    if (s.toLowerCase().equals(value)) {
                        isValid = false;
                    }
                }
                break;
            }

            case 2: {
                for (String s : tastes) {
                    if (s.toLowerCase().equals(value)) {
                        isValid = false;
                    }
                }
                break;
            }

            case 3: {
                for (String s : consistencies) {
                    if (s.toLowerCase().equals(value)) {
                        isValid = false;
                    }
                }
                break;
            }

        }
        return isValid;
    }
    public void clearInputField(View view) {
        input.setText("");
    }
    private void showToast(TextView tv, String message) {
        Toast.makeText(
                getApplicationContext(),
                tv.getText().toString() + " " + message,
                Toast.LENGTH_SHORT
        ).show();
    }
}