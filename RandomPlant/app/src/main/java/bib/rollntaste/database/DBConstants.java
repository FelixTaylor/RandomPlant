package bib.rollntaste.database;

public class DBConstants {
    public static final String DBNAME = "random.db";
    public static final int DBVERSION = 1;

    public static final String ID = "_id";
    public static final String NAME = "_name";

    private static final String SQL_SELECT_ALL = "SELECT * FROM ";


    // TASTE
    public static final String TASTETABLE = "taste";
    public static final String TASTEID = TASTETABLE + ID;
    public static final String TASTENAME = TASTETABLE + NAME;

    public static final String SQL_CREATE_TASTE =
            "CREATE TABLE IF NOT EXISTS " + TASTETABLE + "(" +
                    TASTEID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TASTENAME + " TEXT UNIQUE NOT NULL);";

    public static final String SQL_SELECT_TASTE = SQL_SELECT_ALL + TASTETABLE;
    public static final String SQL_DROP_TASTE = "DROP TABLE " + TASTETABLE;


    // COLOR
    public static final String COLORTABLE = "color";
    public static final String COLORID = COLORTABLE + ID;
    public static final String COLORNAME = COLORTABLE + NAME;

    public static final String SQL_CREATE_COLOR =
            "CREATE TABLE IF NOT EXISTS " + COLORTABLE + "(" +
                    COLORID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLORNAME + " TEXT UNIQUE NOT NULL);";

    public static final String SQL_SELECT_COLOR = SQL_SELECT_ALL + COLORTABLE;
    public static final String SQL_DROP_COLOR = "DROP TABLE " + COLORTABLE;

    // PART
    public static final String PARTTABLE = "part";
    public static final String PARTID = PARTTABLE + ID;
    public static final String PARTNAME = PARTTABLE + NAME;

    public static final String SQL_CREATE_PART =
            "CREATE TABLE IF NOT EXISTS " + PARTTABLE + "(" +
                    PARTID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PARTNAME + " TEXT UNIQUE NOT NULL);";

    public static final String SQL_SELECT_PART = SQL_SELECT_ALL + PARTTABLE;
    public static final String SQL_DROP_PART = "DROP TABLE " + PARTTABLE;

    // CONSISTENCY
    public static final String CONSISTENCYTABLE = "consistency";
    public static final String CONSISTENCYID = CONSISTENCYTABLE + ID;
    public static final String CONSISTENCYNAME = CONSISTENCYTABLE + NAME;

    public static final String SQL_CREATE_CONSISTENCY =
            "CREATE TABLE IF NOT EXISTS " + CONSISTENCYTABLE + "(" +
                    CONSISTENCYID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    CONSISTENCYNAME + " TEXT UNIQUE NOT NULL);";

    public static final String SQL_SELECT_CONSISTENCY = SQL_SELECT_ALL + CONSISTENCYTABLE;
    public static final String SQL_DROP_CONSISTENCY = "DROP TABLE " + CONSISTENCYTABLE;
}
