package bib.rollntaste.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class Handler {
    private static final String LOGTAG = "Handler";
    private DBManager dbManager;
    private SQLiteDatabase db;
    private Context context;

    public Handler(Context context) {
        this.context = context;
        Log.d(LOGTAG, "Handler ready");
    }
    public void open() {
        this.dbManager = new DBManager(this.context);
        this.db = dbManager.getWritableDatabase();
        Log.d(LOGTAG, "The connection to database " + dbManager.getDatabaseName() + " is active.");
    }
    public void close() {
        dbManager.close();
        Log.d(LOGTAG, " Database connection has been closed.");
    }

    public void reset() {
        dbManager.drop(db);
        dbManager.dbinit(db);
    }

    public String insertTaste(String taste) {
        if (taste != null && !taste.trim().isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(DBConstants.TASTENAME, taste);
            db.insert(DBConstants.TASTETABLE,null, values);
        }
        return taste + " was inserted into "+ DBConstants.TASTETABLE;
    }
    public String insertColor(String color) {
        if (color != null && !color.trim().isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(DBConstants.COLORNAME, color);
            db.insert(DBConstants.COLORTABLE,null, values);
        }
        return color + " was inserted into "+ DBConstants.COLORTABLE;
    }
    public String insertPart(String part) {
        if (part != null && !part.trim().isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(DBConstants.PARTNAME, part);
            db.insert(DBConstants.PARTTABLE,null, values);
        }
        return part + " was inserted into "+ DBConstants.PARTTABLE;
    }
    public String insertConsistency(String consistency) {
        if (consistency != null && !consistency.trim().isEmpty()) {
            ContentValues values = new ContentValues();
            values.put(DBConstants.CONSISTENCYNAME, consistency);
            db.insert(DBConstants.CONSISTENCYTABLE,null, values);
        }
        return consistency + " was inserted into "+ DBConstants.CONSISTENCYTABLE;
    }

    public ArrayList<String> selectTaste() {
        ArrayList<String> entities = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SQL_SELECT_TASTE, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SQL_SELECT_TASTE);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            String s = cursor.getString(cursor.getColumnIndex(DBConstants.TASTENAME));
            Log.d(LOGTAG, "FOUND: " + s);
            entities.add(s);
            cursor.moveToNext();
        }
        return entities;
    }
    public ArrayList<String> selectColor() {
        ArrayList<String> entities = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SQL_SELECT_COLOR, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SQL_SELECT_COLOR);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            String s = cursor.getString(cursor.getColumnIndex(DBConstants.COLORNAME));
            Log.d(LOGTAG, "FOUND: " + s);
            entities.add(s);
            cursor.moveToNext();
        }
        return entities;
    }
    public ArrayList<String> selectPart() {
        ArrayList<String> entities = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SQL_SELECT_PART, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SQL_SELECT_PART);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            String s = cursor.getString(cursor.getColumnIndex(DBConstants.PARTNAME));
            Log.d(LOGTAG, "FOUND: " + s);
            entities.add(s);
            cursor.moveToNext();
        }
        return entities;
    }
    public ArrayList<String> selectConsistency() {
        ArrayList<String> entities = new ArrayList<>();
        Cursor cursor = db.rawQuery(DBConstants.SQL_SELECT_CONSISTENCY, null);
        Log.d(LOGTAG, "STATEMENT: " + DBConstants.SQL_SELECT_CONSISTENCY);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            String s = cursor.getString(cursor.getColumnIndex(DBConstants.CONSISTENCYNAME));
            Log.d(LOGTAG, "FOUND: " + s);
            entities.add(s);
            cursor.moveToNext();
        }
        return entities;
    }
}
