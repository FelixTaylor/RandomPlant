package bib.rollntaste.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBManager extends SQLiteOpenHelper {
    private static final String LOGTAG = "DBManager";
    public DBManager(Context context) {
        super(context, DBConstants.DBNAME, null, DBConstants.DBVERSION);
        Log.d(LOGTAG, "Created DB and opened:" + this.getDatabaseName());
    }
    public void drop(SQLiteDatabase db) {
        db.execSQL(DBConstants.SQL_DROP_TASTE);
        db.execSQL(DBConstants.SQL_DROP_COLOR);
        db.execSQL(DBConstants.SQL_DROP_PART);
        db.execSQL(DBConstants.SQL_DROP_CONSISTENCY);
    }
    @Override public void onCreate(SQLiteDatabase db) {
        dbinit(db);
    }
    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void dbinit(SQLiteDatabase db) {
        db.execSQL(DBConstants.SQL_CREATE_TASTE);
        db.execSQL(DBConstants.SQL_CREATE_COLOR);
        db.execSQL(DBConstants.SQL_CREATE_PART);
        db.execSQL(DBConstants.SQL_CREATE_CONSISTENCY);

        Log.d(LOGTAG, "ONCREATE!");
        insert( db,
                DBConstants.TASTETABLE, DBConstants.TASTENAME,
                new String[]{"Bitter", "Feurig", "Herb", "Mentol", "Moderig", "Umami",
                        "Sauer", "Scharf", "Sehr Scharf", "Süß", "Super Süß",
                        "Würzig", "Wässrig"
                }
        );

        insert( db,
                DBConstants.COLORTABLE, DBConstants.COLORNAME,
                new String[]{"Rot", "Grau", "Silber", "Weiß", "Grün", "Olivgrün",
                        "Blau", "Türkis", "Lila", "Gelb", "Braun", "Orange",
                        "Pink", "Beige", "Gelbbraun", "Hellgrün", "Dunkelgrün", "Dungelgrau",
                        "Dunkelrot"
                }
        );

        insert( db,
                DBConstants.PARTTABLE, DBConstants.PARTNAME,
                new String[]{"Wurzel", "Blatt", "Blüte", "Frucht", "Beere", "Nuss",
                        "Stachel", "Fasern", "Pollen"
                }
        );

        insert( db,
                DBConstants.CONSISTENCYTABLE, DBConstants.CONSISTENCYNAME,
                new String[]{"Fest", "Weich", "Bröselig", "Gliberig", "Mukös", "Zäh",
                        "Zähflüssig", "Hart", "Sandig", "Spröde", "Lehmig", "Zart", "Faserig",
                        "Ölig"
                }
        );
    }
    private void insert(SQLiteDatabase db, String table, String column, String[] values) {
        for (int i=0; i<values.length; i++) {
            ContentValues value = new ContentValues();
            value.put(column, values[i]);

            db.insert(table,null,value);
            Log.d(LOGTAG, "INSERTED: " + value);
        }
    }
}
